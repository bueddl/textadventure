#include <fmt/format.h>
#include <functional>
#include <iostream>
#include <istream>
#include <memory>
#include <optional>
#include <sstream>
#include <stack>
#include <string>
#include <utility>
#include <variant>
#include <vector>

class Game;

class Command
{
public:
  explicit Command(Game &game) : game_(game) {}

  virtual ~Command() = default;

  virtual void execute() = 0;

protected:
  Game &game_;
};

class UserInput
{
public:
  struct Object
  {
    std::string noun;
    std::optional<std::string> preposition;
    std::vector<std::string> adjectives;
    std::optional<std::string> article;
  };

  struct Sentence
  {
    std::optional<std::string> adverb;
    std::optional<std::string> verb;
    std::optional<std::string> preposition;
    std::vector<Object> objects;

    std::optional<Object const *> directObject() const
    {
      if (objects.empty())
        return {};

      return &objects[0];
    }
  };

  explicit UserInput(Sentence sentence) : sentence_(std::move(sentence)) {}

  Sentence const &sentence() const { return sentence_; }

private:
  Sentence sentence_;
};

class InputTokenRepository
{
public:
  InputTokenRepository()
  {
    adverbs_ = {
        "kindly",
    };

    verbs_ = {
        "get", "go", "look", "examine", "reply", "take", "ask", "unlock", "talk", "say", "ask", "quit",
    };

    prepositions_ = {
        "above", "below", "with", "for", "at", "on", "to", "around", "with",
    };

    articles_ = {
        "the", "a", "an", "some", "all", "any",
    };

    adjectives_ = {
        "rusty", "big", "old", "red", "blue",
    };

    nouns_ = {
        "west",   "north",    "south",  "east",  "house", "dog",  "key",     "door",    "help",
        "knight", "greeting", "inside", "apple", "place", "area", "station", "outside", "inventory",
    };
  }

  std::vector<std::string> const &adverbs() const { return adverbs_; }

  std::vector<std::string> const &verbs() const { return verbs_; }

  std::vector<std::string> const &prepositions() const { return prepositions_; }

  std::vector<std::string> const &articles() const { return articles_; }

  std::vector<std::string> const &adjectives() const { return adjectives_; }

  std::vector<std::string> const &nouns() const { return nouns_; }

private:
  std::vector<std::string> adverbs_;
  std::vector<std::string> verbs_;
  std::vector<std::string> prepositions_;
  std::vector<std::string> articles_;
  std::vector<std::string> adjectives_;
  std::vector<std::string> nouns_;
};

class InputParser
{
  // adjectives := adjective | adjectives adjective
  // object := [article] adjectives noun
  // objects := [preposition] object | objects preposition object
  // sentence := adverb verb [objects] | verb [ preposition | object [adverb] [objects] ] | object
public:
  explicit InputParser(std::string const &input) : tokens_{tokenize(input)} {}

  auto parse() { return parseSentence(); }

private:
  std::vector<std::string> tokens_;
  std::stack<uint32_t> stack_;
  uint32_t pos_ = 0;

  InputTokenRepository const tokenRepository_;

  std::pair<bool, UserInput::Sentence> parseSentence()
  {
    // sentence := adverb verb [objects] | verb [ preposition | object [adverb] [objects] ] | object

    return parseAnyOf([this]() { return parseSentenceType1(); }, [this]() { return parseSentenceType2(); },
                      [this]() { return parseSentenceType3(); });
  }

  std::pair<bool, UserInput::Sentence> parseSentenceType1()
  {
    // sentence := adverb verb [objects]

    save();

    UserInput::Sentence sentence;

    {
      auto [success, adverb] = parseAdverb();
      if (!success) {
        restore();
        return {false, {}};
      }
      sentence.adverb = adverb;
    }

    {
      auto [success, verb] = parseVerb();
      if (!success) {
        restore();
        return {false, {}};
      }
      sentence.verb = verb;
    }

    {
      auto [success, objects] = parseObjects();
      if (success)
        sentence.objects = objects;
    }

    commit();
    return {true, sentence};
  }

  std::pair<bool, UserInput::Sentence> parseSentenceType2()
  {
    // sentence := verb [ preposition | object [adverb] [objects] ]

    save();

    UserInput::Sentence sentence;

    {
      auto [success, verb] = parseVerb();
      if (!success) {
        restore();
        return {false, {}};
      }
      sentence.verb = verb;
    }

    {
      if (auto [success, preposition] = parsePreposition(); success) {
        sentence.preposition = preposition;
      } else if (auto [success, object] = parseObject(); success) {
        sentence.objects.push_back(object);

        {
          auto [success, adverb] = parseAdverb();
          if (success)
            sentence.adverb = adverb;
        }

        {
          auto [success, objects] = parseObjects();
          if (success)
            copy(begin(objects), end(objects), back_inserter(sentence.objects));
        }
      }
    }

    commit();
    return {true, sentence};
  }

  std::pair<bool, UserInput::Sentence> parseSentenceType3()
  {
    // sentence := object

    save();

    UserInput::Sentence sentence;

    {
      auto [success, object] = parseObject();
      if (!success) {
        restore();
        return {false, {}};
      }

      sentence.objects.push_back(object);
    }

    commit();
    return {true, sentence};
  }

  std::pair<bool, std::vector<UserInput::Object>> parseObjects()
  {
    // objects := [preposition] object | objects preposition object

    save();

    std::vector<UserInput::Object> objects;

    auto [success, object] = parseObject();
    if (!success) {
      restore();
      return {false, {}};
    }

    while (true) {
      objects.push_back(object);

      auto [success, object] = parseObject();
      if (!success || !object.preposition)
        break;
    }

    commit();
    return {true, objects};
  }

  std::pair<bool, UserInput::Object> parseObject()
  {
    // object := [preposition] [article] [adjectives] noun

    save();

    UserInput::Object object;

    {
      auto [success, preposition] = parsePreposition();
      if (success)
        object.preposition = preposition;
    }

    {
      auto [success, article] = parseArticle();
      if (success)
        object.article = article;
    }

    {
      auto [success, adjectives] = parseAdjectives();
      if (success)
        object.adjectives = std::move(adjectives);
    }

    {
      auto [success, noun] = parseNoun();
      if (!success) {
        restore();
        return {false, {}};
      }
      object.noun = noun;
    }

    commit();
    return {true, object};
  }

  std::pair<bool, std::vector<std::string>> parseAdjectives()
  {
    // adjectives := adjective | adjectives adjective

    save();

    std::vector<std::string> adjectives;

    while (true) {
      auto [success, adjective] = parseAdjective();
      if (!success)
        break;

      adjectives.push_back(adjective);
    }

    if (adjectives.empty()) {
      restore();
      return {false, {}};
    }

    commit();
    return {true, adjectives};
  }

  template<typename P, typename... Ps>
  auto parseAnyOf(P &&parserExpression, Ps &&... parserExpressions) -> decltype(parserExpression())
  {
    if (auto [success, value] = parserExpression(); success)
      return {success, value};

    if constexpr (sizeof...(parserExpressions) >= 1)
      return parseAnyOf(std::forward<Ps>(parserExpressions)...);

    return {false, {}};
  }

  std::pair<bool, std::string> parseAdverb() { return anyOf(tokenRepository_.adverbs()); }

  std::pair<bool, std::string> parseVerb() { return anyOf(tokenRepository_.verbs()); }

  std::pair<bool, std::string> parsePreposition() { return anyOf(tokenRepository_.prepositions()); }

  std::pair<bool, std::string> parseArticle() { return anyOf(tokenRepository_.articles()); }

  std::pair<bool, std::string> parseAdjective() { return anyOf(tokenRepository_.adjectives()); }

  std::pair<bool, std::string> parseNoun() { return anyOf(tokenRepository_.nouns()); }

  std::pair<bool, std::string> anyOf(std::vector<std::string> const &words)
  {
    if (!tokenAvailable())
      return {false, {}};

    save();
    auto current = currentSeek();

    for (auto const &word : words)
      if (word == current) {
        commit();
        return {true, word};
      }

    restore();
    return {false, {}};
  }

  void save() { stack_.push(pos_); }

  void commit() { stack_.pop(); }

  void restore()
  {
    pos_ = stack_.top();
    stack_.pop();
  }

  std::string const &current() const { return tokens_[pos_]; }

  void seek() { ++pos_; }

  std::string currentSeek()
  {
    auto token = tokens_[pos_];
    seek();
    return token;
  }

  bool tokenAvailable() const { return pos_ < tokens_.size(); }

  std::vector<std::string> tokenize(const std::string &str, char delimiter = ' ')
  {
    std::vector<std::string> strings;
    for (size_t first = 0, last = 0; last < str.length(); first = last + 1) {
      last = str.find(delimiter, first);
      strings.push_back(str.substr(first, last - first));
    }
    return strings;
  }
};

class Game;

class ICommandBuilder
{
public:
  virtual ~ICommandBuilder() = default;

  virtual std::unique_ptr<Command> build(Game &game, UserInput const &userInput) const = 0;
};

namespace matcher
{
template<typename T, typename F>
class Selector
{
public:
  using result_type = T;
  using selector_fn_t = F;

  constexpr explicit Selector(selector_fn_t fn) : fn_{std::move(fn)} {}

  auto operator()(UserInput const &userInput) const { return fn_(userInput); }

private:
  selector_fn_t fn_;
};

template<typename T>
struct IsSelector : std::false_type
{};

template<typename T, typename F>
struct IsSelector<Selector<T, F>> : std::true_type
{};

template<typename T>
constexpr auto IsSelector_v = IsSelector<T>::value;

template<typename T, typename = typename T::value_type>
std::true_type _HasValueType(T);
std::false_type _HasValueType(...);

template<typename T>
struct HasValueType : decltype(_HasValueType(std::declval<T>()))
{};

template<typename T>
constexpr auto HasValueType_v [[maybe_unused]] = HasValueType<T>::value;

template<typename T>
struct IsIterator : HasValueType<std::iterator_traits<T>>
{};

template<typename T>
constexpr auto IsIterator_v = IsIterator<T>::value;

template<typename T>
struct IsIteratorRange : std::false_type
{};

template<typename Iter>
struct IsIteratorRange<std::pair<Iter, Iter>> : std::bool_constant<IsIterator_v<Iter>>
{};

template<typename T>
constexpr auto IsIteratorRange_v = IsIteratorRange<T>::value;

template<typename T>
struct IsOptional : std::false_type
{};

template<typename T>
struct IsOptional<std::optional<T>> : std::true_type
{};

template<typename T>
constexpr auto IsOptional_v = IsOptional<T>::value;

template<typename F>
constexpr auto makeSelector(F fn) noexcept
{
  using result_type = std::invoke_result_t<F, UserInput>;
  return Selector<result_type, F>{fn};
}

constexpr auto const verb = makeSelector([](UserInput const &userInput) { return userInput.sentence().verb; });
constexpr auto const object = makeSelector([](UserInput const &userInput) {
  return std::pair{begin(userInput.sentence().objects), end(userInput.sentence().objects)};
});

template<typename R, typename F, typename T>
constexpr auto operator==(Selector<R, F> const &selector, T const &value)
{
  return makeSelector([&selector, value](UserInput const &userInput) {
    if constexpr (IsIteratorRange_v<R>) {
      auto [begin, end] = selector(userInput);
      return find_if(begin, end, [&value](auto const &object) { return object.noun == value; }) != end;
    } else if (IsOptional_v<R>) {
      auto optional = selector(userInput);
      if (!optional.has_value())
        return false;
      return optional.value() == value;
    } else {
      return selector(userInput) == value;
    }
  });
}

template<typename F0, typename F1>
constexpr auto operator&&(Selector<bool, F0> const &lhs, Selector<bool, F1> const &rhs)
{
  return makeSelector([lhs, rhs](UserInput const &userInput) { return lhs(userInput) && rhs(userInput); });
}

template<typename F0, typename F1>
constexpr auto operator||(Selector<bool, F0> const &lhs, Selector<bool, F1> const &rhs)
{
  return makeSelector([lhs, rhs](UserInput const &userInput) { return lhs(userInput) || rhs(userInput); });
}

template<typename T, typename F,
         typename = std::enable_if_t<std::is_same_v<T, bool> || IsOptional_v<T> || IsIteratorRange_v<T>>>
constexpr auto operator!(Selector<T, F> const &expr)
{
  return makeSelector([expr](UserInput const &userInput) {
    if constexpr (IsIteratorRange_v<T>) {
      auto [begin, end] = expr(userInput);
      return begin == end;
    } else {
      return !expr(userInput);
    }
  });
}

namespace v2
{
class matcher
{
public:
  matcher() : expr_{[](UserInput const &userInput [[maybe_unused]]) { return true; }} {}

  matcher &objectIs(std::string const &objectId)
  {
    auto expr = std::move(expr_);
    expr_ = makeSelector(expr) && object == objectId;
    return *this;
  }

  template<typename... Ts>
  matcher &isAnyOfVerb(Ts &&... args)
  {
    auto expr = std::move(expr_);
    expr_ = makeSelector(expr) && (... || (verb == args));
    return *this;
  }

  matcher &verbIs(std::string const &verbId)
  {
    auto expr = std::move(expr_);
    expr_ = makeSelector(expr) && verb == verbId;
    return *this;
  }

  template<typename... Ts>
  matcher &verbIsNoneOrAnyOf(Ts &&... args)
  {
    auto expr = std::move(expr_);
    expr_ = makeSelector(expr) && (!verb || ... || (verb == args));
    return *this;
  }

  std::function<bool(UserInput const &)> expression() const { return expr_; }

private:
  std::function<bool(UserInput const &)> expr_;
};

}  // namespace v2

}  // namespace matcher

class Matcher
{
public:
  explicit Matcher(std::function<bool(UserInput const &)> expr) : expr_{std::move(expr)} {}

  auto &on(std::function<bool(UserInput const &)> f)
  {
    auto expr = std::move(expr_);
    expr_ = matcher::makeSelector(expr) || matcher::makeSelector(std::move(f));
    return *this;
  }

  void then(std::unique_ptr<ICommandBuilder> builder) { builder_ = std::move(builder); }

  bool matches(UserInput const &userInput) const { return expr_(userInput); }

  ICommandBuilder const &builder() const { return *builder_; }

private:
  std::unique_ptr<ICommandBuilder> builder_;
  std::function<bool(UserInput const &)> expr_;
};

class Console
{
public:
  Console(std::istream &in, std::ostream &out) : in_{in}, out_{out} {}

  std::string readLine()
  {
    std::string str;
    std::getline(in_ >> std::ws, str);
    return str;
  }

  template<typename... As>
  void write(std::string const &format, As &&... args) const
  {
    out_ << fmt::format(format, std::forward<As>(args)...);
  }

  template<typename... As>
  void writeLine(std::string const &format, As &&... args) const
  {
    write(format + "\n", std::forward<As>(args)...);
  }

  void clearLine() const { write("\n"); }

private:
  std::istream &in_;
  std::ostream &out_;
};

class UserConsole : public Console
{
public:
  static constexpr auto prompt_ = "> ";

  using Console::Console;

  template<typename F>
  auto &on(F const f)
  {
    if constexpr (matcher::IsSelector_v<F>) {
      std::function<bool(UserInput const &)> fn = f;
      return matchers_.emplace_back(std::move(fn));
    } else {
      std::function<void(matcher::v2::matcher &)> matcherBuilder = f;
      matcher::v2::matcher matcher;
      matcherBuilder(matcher);
      return matchers_.emplace_back(matcher.expression());
    }
  }

  void processNextUserInput(Game &game)
  {
    write("> ");

    InputParser inputParser{readLine()};
    auto [success, sentence] = inputParser.parse();
    if (!success) {
      writeLine("Sorry, I did not understand this.");
      return;
    }
    UserInput userInput{sentence};

    for (auto const &matcher : matchers_) {
      if (matcher.matches(userInput)) {
        auto command = matcher.builder().build(game, userInput);
        if (command != nullptr)
          command->execute();
        return;
      }
    }

    writeLine("Sorry, I do not know how to do this.");
  }

private:
  std::vector<Matcher> matchers_;
};

class DescribableGameObject
{
public:
  enum class ArticleStyle
  {
    Indefinite,
    Definite,
  };

  DescribableGameObject() = default;
  explicit DescribableGameObject(std::string noun, std::vector<std::string> adjectives = {})
      : noun_{std::move(noun)}, adjectives_{std::move(adjectives)}
  {}

  bool matchesDescription(UserInput::Object const &description) const noexcept
  {
    if (noun_ != description.noun)
      return false;

    for (auto const &requiredAdjective : description.adjectives)
      if (find(begin(adjectives_), end(adjectives_), requiredAdjective) == end(adjectives_))
        return false;

    return true;
  }

  std::string describe(ArticleStyle articleStyle = ArticleStyle::Indefinite) const
  {
    std::string description;

    if (articleStyle == ArticleStyle::Indefinite) {
      auto isVocal = [](char ch) {
        char vocals[] = {'a', 'e', 'i', 'o', 'u'};

        using std::begin;
        using std::end;
        using std::find;

        return find(begin(vocals), end(vocals), ch) != end(vocals);
      };

      char firstLetter;
      if (adjectives_.empty())
        firstLetter = noun_[0];
      else
        firstLetter = adjectives_[0][0];

      if (isVocal(firstLetter))
        description += "an";
      else
        description += "a";
    } else {
      description += "the";
    }

    for (auto const &adjective : adjectives_)
      description += " " + adjective;

    description += " " + noun_;

    return description;
  }

protected:
  auto &setNoun(std::string noun) noexcept
  {
    noun_ = std::move(noun);
    return *this;
  }

  auto &addAdjective(std::string adjective) noexcept
  {
    adjectives_.push_back(std::move(adjective));
    return *this;
  }

private:
  std::string noun_;  // TODO: multiple accepted nouns?
  std::vector<std::string> adjectives_;
};

class Connection : public DescribableGameObject
{
public:
  Connection() : DescribableGameObject("door") {}

  std::string const &name() const { return name_; }

  virtual bool canEnter() const { return true; }

private:
  std::string name_;
  // Room...
};

class Key : public DescribableGameObject
{
public:
  Key(std::vector<std::string> adjectives) : DescribableGameObject{"key", adjectives} {}
};

class LockedByItemConnection : public Connection
{
public:
  explicit LockedByItemConnection(Key &lockedByItem) : lockedByItem_(lockedByItem) { addAdjective("red"); }

  bool canEnter() const override { return !locked_; }

  bool unlock(Key const &key)
  {
    if (&key == &lockedByItem_)
      locked_ = false;

    return canEnter();
  }

private:
  Key &lockedByItem_;
  bool locked_ = true;
};

using GameObject = std::variant<Connection, LockedByItemConnection, Key>;

class UnlockCommand : public Command
{
public:
  UnlockCommand(Game &game, LockedByItemConnection &door, Key &key) : Command{game}, door_{door}, key_{key} {}

  void execute() override;

private:
  LockedByItemConnection &door_;
  Key &key_;
};

class UnlockCommandBuilder : public ICommandBuilder
{
public:
  std::unique_ptr<Command> build(Game &game, UserInput const &userInput) const override;
};

class ListInventoryCommand : public Command
{
public:
  using Command::Command;

  void execute() override;
};

class ListInventoryCommandBuilder : public ICommandBuilder
{
public:
  std::unique_ptr<Command> build(Game &game, UserInput const &userInput [[maybe_unused]]) const override
  {
    return std::make_unique<ListInventoryCommand>(game);
  }
};

class QuitGameCommand : public Command
{
public:
  using Command::Command;

  void execute() override;
};

class QuitGameCommandBuilder : public ICommandBuilder
{
public:
  std::unique_ptr<Command> build(Game &game, UserInput const &userInput [[maybe_unused]]) const override
  {
    return std::make_unique<QuitGameCommand>(game);
  }
};

GameObject key0;  // DUMMY
GameObject key1;  // DUMMY

class World
{
public:
  World()
  {
    key0.emplace<Key>(std::vector<std::string>{"rusty", "old"});
    key1.emplace<Key>(std::vector<std::string>{"old"});

    {
      auto object = new GameObject;
      object->emplace<Connection>();
      objects_.push_back(object);
    }

    {
      auto object = new GameObject;
      object->emplace<LockedByItemConnection>(std::get<Key>(key0));
      objects_.push_back(object);
    }
  }

  auto begin() const
  {
    using std::begin;
    return begin(objects_);
  }

  auto end() const
  {
    using std::end;
    return end(objects_);
  }

private:
  std::vector<GameObject *> objects_;
};

class Inventory
{
public:
  Inventory()
  {
    objects_.push_back(&key0);
    objects_.push_back(&key1);
  }

  auto begin() const noexcept
  {
    using std::begin;
    return begin(objects_);
  }

  auto end() const noexcept
  {
    using std::end;
    return end(objects_);
  }

  bool empty() const noexcept { return objects_.empty(); }

  auto count() const noexcept { return objects_.size(); }

private:
  std::vector<GameObject *> objects_;
};

class Game
{
public:
  Game() : console_{std::cin, std::cout} { init(); }

  void run()
  {
    do
      console_.processNextUserInput(*this);
    while (keepRunning_);
  }

  void stop() { keepRunning_ = false; }

  World const &world() const { return world_; }

  Inventory const &inventory() const { return inventory_; }

  UserConsole const &console() const { return console_; }

private:
  UserConsole console_;
  bool keepRunning_ = true;

  World world_;
  Inventory inventory_;

  void init()
  {
    using matcher::object;
    using matcher::verb;

    // clang-format off

    console_
      .on(verb == "unlock")
      .then(std::make_unique<UnlockCommandBuilder>());

    console_
      .on(verb == "quit" && (!object || object == "game"))
      .then(std::make_unique<QuitGameCommandBuilder>());

    console_
      .on(object == "inventory" && (!verb || verb == "examine" || verb == "list"))
      .then(std::make_unique<ListInventoryCommandBuilder>());

    // clang-format on
  }
};

template<typename... Fs>
struct overload : Fs...
{
  using Fs::operator()...;
};

template<typename... Fs>
overload(Fs...)->overload<Fs...>;

template<typename T, typename ObjectCollection>
std::pair<std::size_t, std::vector<T *>> fetchObjects(ObjectCollection const &objectCollection,
                                                      std::optional<UserInput::Object> const &description) noexcept
{
  std::vector<T *> candidates;
  for (auto &object : objectCollection) {
    if (!std::holds_alternative<T>(*object))
      continue;

    auto &filteredObject = std::get<T>(*object);
    if constexpr (std::is_base_of_v<DescribableGameObject, T>) {
      if (description.has_value() && !filteredObject.matchesDescription(description.value()))
        continue;
    }

    candidates.push_back(&filteredObject);
  }

  return {candidates.size(), candidates};
}

class InputValidator
{
public:
  enum class ObjectSource
  {
    World,
    Inventory,
  };

  enum class ValidationResult
  {
    Success,
    NoDescription,
    NoMatch,
    MultipleMatches,
  };

  explicit InputValidator(Game &game, UserInput const &userInput) : game_(game), userInput_(userInput) {}

  template<typename T>
  std::pair<ValidationResult, T *> validateObject(ObjectSource source)
  {
    std::optional<UserInput::Object> description;
    if (userInput_.sentence().objects.size() >= objectIndex_ + 1)
      description = userInput_.sentence().objects[objectIndex_];

    std::optional<T *> object;
    switch (source) {
    case ObjectSource::World:
      object = fetchObjectFromWorld<T>(game_, description);
      break;
    case ObjectSource::Inventory:
      object = fetchObjectFromInventory<T>(game_, description);
      break;
    }

    ++objectIndex_;

    if (!object) {
      if (!description)
        return {ValidationResult::NoDescription, nullptr};
      else
        return {ValidationResult::NoMatch, nullptr};
    }

    return {ValidationResult::Success, object.value()};
  }

private:
  Game &game_;

  UserInput const &userInput_;
  std::size_t objectIndex_ = 0;

  template<typename T, typename ObjectCollection>
  std::optional<T *>
  fetchObject(ObjectCollection const &objectCollection, std::optional<UserInput::Object> const &description = {},
              std::function<void(std::size_t count, std::vector<T *> const &candidates)> callback = [](auto, auto) {})
  {
    auto [count, candidates] = fetchObjects<T>(objectCollection, description);

    callback(count, candidates);
    if (count == 1)
      return candidates[0];

    return {};
  }

  template<typename T>
  std::optional<T *> fetchObjectFromWorld(Game &game, std::optional<UserInput::Object> const &description = {})
  {
    auto print = [&](auto count, auto const &candidates) {
      switch (count) {
      case 1:
        break;

      case 0:
        game.console().writeLine("You don't see anything like that around you!");
        break;

      default:
        if constexpr (std::is_base_of_v<DescribableGameObject, T>) {
          game.console().writeLine("You see multiple such objects:");
          for (auto const &object : candidates)
            game.console().writeLine("  {}", object->describe());
          game.console().clearLine();
        } else {
          game.console().writeLine("You see multiple such objects.");
        }
        break;
      }
    };

    if (!description)
      return fetchObject<T>(game.world());
    return fetchObject<T>(game.world(), description, print);
  }

  template<typename T>
  std::optional<T *> fetchObjectFromInventory(Game &game, std::optional<UserInput::Object> const &description = {})
  {
    auto print = [&](auto count, auto const &candidates) {
      switch (count) {
      case 1:
        break;

      case 0:
        game.console().writeLine("You don't have anything like that with you!");
        break;

      default:
        if constexpr (std::is_base_of_v<DescribableGameObject, T>) {
          game.console().writeLine("You have multiple such objects:");
          for (auto const &object : candidates)
            game.console().writeLine("  {}", object->describe());
          game.console().clearLine();
        } else {
          game.console().writeLine("You have multiple such objects.");
        }
        break;
      }
    };

    if (!description)
      return fetchObject<T>(game.inventory());
    return fetchObject<T>(game.inventory(), description, print);
  }
};

void QuitGameCommand::execute() { game_.stop(); }

std::unique_ptr<Command> UnlockCommandBuilder::build(Game &game, UserInput const &userInput) const
{
  InputValidator validator{game, userInput};

  auto [doorFound, door] = validator.validateObject<LockedByItemConnection>(InputValidator::ObjectSource::World);
  switch (doorFound) {
  case InputValidator::ValidationResult::Success:
    break;

  case InputValidator::ValidationResult::NoDescription:
    game.console().writeLine("Unlock what?");
    [[fallthrough]];

  default:
    return {};
  }

  auto [keyFound, key] = validator.validateObject<Key>(InputValidator::ObjectSource::Inventory);
  switch (keyFound) {
  case InputValidator::ValidationResult::Success:
    break;

  case InputValidator::ValidationResult::NoDescription:
    game.console().writeLine("Unlock {} with what?", door->describe(DescribableGameObject::ArticleStyle::Definite));
    [[fallthrough]];

  default:
    return {};
  }

  return std::make_unique<UnlockCommand>(game, *door, *key);
}

void UnlockCommand::execute()
{
  game_.console().write("You are turning the key... ");
  if (door_.unlock(key_))
    game_.console().writeLine("you hear a click and the door opens.");
  else
    game_.console().writeLine("however - this seems to have no effect. The door remains locked.");
}

void ListInventoryCommand::execute()
{
  if (game_.inventory().empty()) {
    game_.console().writeLine("You are not carrying anything with you.");
    return;
  }

  game_.console().writeLine("You look into your pockets and see {} things you are carrying with you:",
                            game_.inventory().count());
  for (auto const &object : game_.inventory()) {
    visit(overload{[&](DescribableGameObject const &object) { game_.console().writeLine("  {}", object.describe()); },
                   [&](auto const &) {
                     game_.console().writeLine("  you have no clue how to describe that thing...whatever it is");
                   }},
          *object);
  }
}

int main()
{
  Game game;
  game.run();
}
